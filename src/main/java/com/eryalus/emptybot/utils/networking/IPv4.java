/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.emptybot.utils.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.StringTokenizer;
import com.eryalus.emptybot.utils.numeric.Transforms;

/**
 *
 * @author eryalus
 */
public class IPv4 {

    /**
     * Get Lan interface's IP or Public IP if it's not inside a Lan.
     *
     * @return Lan interface's IP. null if there's no such IP
     * @throws IOException If an IO error occurs.
     */
    public static InetAddress getLansIP() throws IOException {
        return getIPFromDefaultGateway(getDefaultGateway());
    }

    private static int MaskLengthToBits(Integer length) {
        int t = 0;
        for (int i = 0; i < length; i++) {
            t = t | (1 << (31 - i));
        }
        return t;
    }

    /**
     * Checks if the two give IPs starts with the same mask_length bits (same
     * lan)
     *
     * @param addr1 First address
     * @param addr2 Second address
     * @param mask_length Mask length
     * @return
     */
    public static boolean checkSameLan(InetAddress addr1, InetAddress addr2, Integer mask_length) {
        return checkSameLan(addr1.getAddress(), addr2.getAddress(), mask_length);
    }

    /**
     * Checks if the two give IPs starts with the same mask_length bits (same
     * lan)
     *
     * @param addr1 First address
     * @param addr2 Second address
     * @param mask_length Mask length
     * @return
     */
    public static boolean checkSameLan(byte[] addr1, byte[] addr2, Integer mask_length) {
        int or = MaskLengthToBits(mask_length);
        int here = Transforms.toInt(addr1);
        int other = Transforms.toInt(addr2);
        return (here & or) == (other & or);
    }

    private static InetAddress getIPFromDefaultGateway(String default_gateway) throws SocketException, UnknownHostException {
        byte[] def_gat = Inet4Address.getByName(default_gateway).getAddress();
        for (final Enumeration< NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
                interfaces.hasMoreElements();) {
            final NetworkInterface cur = interfaces.nextElement();

            if (cur.isLoopback()) {
                continue;
            }

            for (final InterfaceAddress addr : cur.getInterfaceAddresses()) {
                final InetAddress inet_addr = addr.getAddress();

                if (!(inet_addr instanceof Inet4Address)) {
                    continue;
                }

                byte[] addr_b = addr.getAddress().getAddress();

                int length = addr.getNetworkPrefixLength();
                if (checkSameLan(addr_b, def_gat, length)) {
                    return addr.getAddress();
                }

            }
        }
        return null;
    }

    private static String getDefaultGateway() throws IOException {
        Process result = Runtime.getRuntime().exec("netstat -rn");
        BufferedReader output = new BufferedReader(new InputStreamReader(result.getInputStream()));
        String thisLine;
        while ((thisLine = output.readLine()) != null) {
            StringTokenizer st = new StringTokenizer(thisLine);
            if (!st.hasMoreTokens()) {
                continue;
            }
            st.nextToken();
            String genmask = "";
            String gateway = "";
            if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                if (!st.hasMoreTokens()) {
                    continue;
                }
                genmask = st.nextToken();
                if (!st.hasMoreTokens()) {
                    continue;
                }
                gateway = st.nextToken();
            } else {
                if (!st.hasMoreTokens()) {
                    continue;
                }
                gateway = st.nextToken();
                if (!st.hasMoreTokens()) {
                    continue;
                }
                genmask = st.nextToken();
            }
            if (genmask.equals("0.0.0.0")) {
                return gateway;
            }

        }
        return null;
    }
}
