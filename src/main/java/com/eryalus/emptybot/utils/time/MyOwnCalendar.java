/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.emptybot.utils.time;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author eryalus
 */
public class MyOwnCalendar extends GregorianCalendar {

    /**
     * Months' names
     */
    private static final String[][] MONTHS = new String[][]{
        new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"},
        new String[]{"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"}
    };

    /**
     * Days' names
     */
    private static final String[][] DAYS = new String[][]{
        new String[]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"},
        new String[]{"Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"}
    };

    /**
     * Formats and languages
     */
    public static final Integer ENGLISH = 0, SPANISH = 1;
    /**
     * Hours formats
     */
    public static final Integer HOURS_12 = 0, HOURS_24 = 1;

    /**
     * Add zeros to the text until it fits the size. e.g. txt="2" and size = 2
     * => output = "02".
     *
     * @param txt Original text before adding zeros.
     * @param size Number of characters.
     * @return
     */
    private String addZeros(String txt, int size) {
        int toadd = size - txt.length();
        if (toadd > 0) {
            String temp = "";
            for (int i = 0; i < toadd; i++) {
                temp += "0";
            }
            temp += txt;
            return temp;
        } else {
            return txt;
        }
    }

    /**
     * Create a new instance with the given time.
     *
     * @param time The time in milliseconds.
     */
    public MyOwnCalendar(Long time) {
        super();
        super.setTimeInMillis(time);
    }

    /**
     * Create a new instance with the time of the Calendar object.
     *
     * @param calendar Object from where the time is obtained.
     */
    public MyOwnCalendar(Calendar calendar) {
        super();
        super.setTimeInMillis(calendar.getTimeInMillis());
    }

    /**
     * Create a new instance with the current time.
     */
    public MyOwnCalendar() {
        super();
        super.setTimeInMillis(Calendar.getInstance().getTimeInMillis());
    }

    /**
     * Get the year.
     *
     * @return Year.
     */
    public Integer getYear() {
        return super.get(Calendar.YEAR);
    }

    /**
     * Get year formatted. If the number of characters of the year are smaller
     * than 'size' then it will add zeros to fit the size. e.g. The year is 1970
     * and size 5 => "01970". If the size is 3 => "1970".
     *
     * @param size Number of minimun characters.
     * @return Formatted Year.
     */
    public String getFormattedYear(Integer size) {
        return addZeros("" + (super.get(Calendar.YEAR)), size);
    }

    /**
     * Get the month.
     *
     * @return Month.
     */
    public Integer getMonth() {
        return super.get(Calendar.MONTH) + 1;
    }

    /**
     * Get year formatted. If the number of characters of the month are smaller
     * than 'size' then it will add zeros to fit the size. e.g. The month is 5
     * and size 2 => "05". If the month is 11 the size is 1 => "11".
     *
     * @param size Number of minimun characters.
     * @return Formatted Month.
     */
    public String getFormattedMonth(Integer size) {
        return addZeros("" + (super.get(Calendar.MONTH) + 1), size);
    }

    /**
     * Get the name of the month.
     *
     * @param Language ENGLISH or SPANISH, Wrong values can cause a
     * NullPointerException.
     * @return Name of the month.
     */
    public String getMonthName(Integer Language) {
        return MONTHS[Language][super.get(Calendar.MONTH)];
    }

    /**
     * Get the day of the month.
     *
     * @return Day of the month.
     */
    public Integer getDay() {
        return super.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Get the day of the month formatted. If the number of characters of the
     * month are smaller than 'size' then it will add zeros to fit the size.
     * e.g. The day is 21 and size 3 => "021". If the size is 1 => "21".
     *
     * @param size Number of minimun characters.
     * @return Formatted Day.
     */
    public String getFormattedDay(Integer size) {
        return addZeros("" + (super.get(Calendar.DAY_OF_MONTH)), size);
    }

    /**
     * Get the day of the week.
     *
     * @param format ENGLISH or SPANISH, with wrong values ENGLISH is used.
     * @return Day of the month.
     */
    public Integer getDayOfWeek(Integer format) {
        if (format.equals(SPANISH)) {
            Integer day = super.get(Calendar.DAY_OF_WEEK);
            switch (day) {
                case 1:
                    return 7;
                default:
                    return day - 1;
            }
        } else {
            return super.get(Calendar.DAY_OF_WEEK);
        }

    }

    /**
     * Get the day of the week formatted. If the number of characters of the day
     * are smaller than 'size' then it will add zeros to fit the size. e.g. The
     * day is 7 and size 3 => "007". If the size is 0 => "7".
     *
     * @param size Number of minimun characters.
     * @param format ENGLISH or SPANISH, with wrong values ENGLISH is used.
     * @return Formatted Day of the week.
     */
    public String getFormattedDayOfWeek(Integer size, Integer format) {
        return addZeros("" + getDayOfWeek(format), size);
    }

    /**
     * Get the name of the day of the week.
     *
     * @param Language ENGLISH or SPANISH, Wrong values can cause a
     * NullPointerException.
     * @return Name of the month.
     */
    public String getDayOfWeekName(Integer Language) {
        return DAYS[Language][super.get(Calendar.MONTH)];
    }

    /**
     * Get the hour of the day. Values [0-23] for HOURS_24 format and [0-11] for
     * HOURS_12 format (Noon and midnight are represented by 0, not by 12).
     *
     * @param format HOURS_24 and HOURS_12, with wrong values HOURS_24 is used.
     * @return Hour.
     */
    public Integer getHour(Integer format) {
        if (format.equals(HOURS_12)) {
            return super.get(Calendar.HOUR);
        } else {
            return super.get(Calendar.HOUR_OF_DAY);
        }
    }

    /**
     * Get the hour of the day formatted. Values [0-23] for HOURS_24 format and
     * [0-11] for HOURS_12 format (Noon and midnight are represented by 0, not
     * by 12). If the number of characters of the hour are smaller than 'size'
     * then it will add zeros to fit the size. e.g. The hour is 21 and size 3 =>
     * "021". If the size is 1 => "21".
     *
     * @param size Number of minimun characters.
     * @param format HOURS_24 and HOURS_12, with wrong values HOURS_24 is used.
     * @return Formatted hour.
     */
    public String getFormattedHour(Integer size, Integer format) {
        return addZeros("" + getHour(format), size);
    }

    /**
     * Get the minutes of the hour. Values [0-59]
     *
     * @return Minutes.
     */
    public Integer getMinutes() {
        return super.get(Calendar.MINUTE);
    }

    /**
     * Get the minutes of the hour formatted. If the number of characters of the
     * minutes are smaller than 'size' then it will add zeros to fit the size.
     * e.g. The minutes are 21 and size 3 => "021". If the size is 1 => "21".
     *
     * @param size Number of minimun characters.
     * @return Formatted minutes.
     */
    public String getFormattedMinutes(Integer size) {
        return addZeros("" + (super.get(Calendar.MINUTE)), size);
    }

    /**
     * Get the seconds of the minute. Values [0-59]
     *
     * @return Seconds.
     */
    public Integer getSeconds() {
        return super.get(Calendar.SECOND);
    }

    /**
     * Get the seconds of the minute formatted. If the number of characters of
     * the seconds are smaller than 'size' then it will add zeros to fit the
     * size. e.g. The seconds are 21 and size 3 => "021". If the size is 1 =>
     * "21".
     *
     * @param size Number of minimun characters.
     * @return Formatted Seconds.
     */
    public String getFormattedSeconds(Integer size) {
        return addZeros("" + (super.get(Calendar.SECOND)), size);
    }

    /**
     * Get the milliseconds of the second. Values [0-999]
     *
     * @return Seconds.
     */
    public Integer getMillis() {
        return super.get(Calendar.MILLISECOND);
    }

    /**
     * Get the milliseconds of the second formatted. If the number of characters
     * of the milliseconds are smaller than 'size' then it will add zeros to fit
     * the size. e.g. The milliseconds are 541 and size 4 => "0541". If the size
     * is 1 => "541".
     *
     * @param size Number of minimun characters.
     * @return Formatted Milliseconds.
     */
    public String getFormattedMillis(Integer size) {
        return addZeros("" + (super.get(Calendar.MILLISECOND)), size);
    }

    /**
     * Returns this Calendar's time value in seconds.
     *
     * @return the current time as UTC seconds from the epoch.
     */
    public Long getTimeInSeconds() {
        return getTimeInMillis() / 1000;
    }

    /**
     * Returns this Calendar's time value in minutes.
     *
     * @return the current time as UTC minutes from the epoch.
     */
    public Long getTimeInMinutes() {
        return getTimeInSeconds() / 60;
    }

    /**
     * Returns this Calendar's time value in hours.
     *
     * @return the current time as UTC hours from the epoch.
     */
    public Long getTimeInHours() {
        return getTimeInMinutes() / 60;
    }

    /**
     * Returns this Calendar's time value in days.
     *
     * @return the current time as UTC days from the epoch.
     */
    public Long getTimeInDays() {
        return getTimeInHours() / 24;
    }

    /**
     * Returns this Calendar's time value in years.
     *
     * @return the current time as UTC years from the epoch.
     */
    public Long getTimeInYears() {
        return (long) ((double) getTimeInDays() / 365.25);
    }

    /**
     * Get a representation of the time. ENGLISH format: [YYYY/DD/MM] | SPANISH
     * format: [YYYY/MM/DD]
     *
     * @param format ENGLISH or SPANISH, with wrong values ENGLISH is used.
     * @return Representation of the time.
     */
    public String getDate(Integer format) {
        if (format.equals(SPANISH)) {
            return "[" + super.get(Calendar.YEAR) + "/" + addZeros("" + (super.get(Calendar.MONTH) + 1), 2) + "/" + addZeros("" + super.get(Calendar.DAY_OF_MONTH), 2) + "]";
        } else {
            return "[" + super.get(Calendar.YEAR) + "/" + addZeros("" + super.get(Calendar.DAY_OF_MONTH), 2) + "/" + addZeros("" + (super.get(Calendar.MONTH) + 1), 2) + "]";
        }
    }

    /**
     * Set the time to this calendar from the output of getDate of another
     * instance. It could cause a lost of precision of the time due to the lack
     * of hour, minute, second and millisecond.
     *
     * @param getDate output of getDate of another instance.
     * @param format the output format of the getDate. ENGLISH or SPANISH, with
     * wrong values ENGLISH is used.
     * @return true if time was setted, false otherwise.
     */
    public boolean reverseGetDate(String getDate, Integer format) {
        if (getDate.length() == "[YYYY/DD/MM]".length()) {
            String copy = getDate.substring(1, getDate.length() - 1); //delete '[' and ']'
            String parts[] = copy.split("/");
            if (parts.length == 3) {
                try {
                    Integer year = Integer.parseInt(parts[0]);
                    Integer month = Integer.parseInt(parts[1]);
                    Integer days = Integer.parseInt(parts[2]);
                    MyOwnCalendar own = new MyOwnCalendar(0L);
                    own.set(0, 0, 0, 0, 0, 0);
                    if (format.equals(SPANISH)) {
                        own.set(year, month - 1, days);
                    } else {
                        own.set(year, days - 1, month);
                    }
                    this.setTimeInMillis(own.getTimeInMillis());
                    return true;
                } catch (NumberFormatException ex) {

                }
            }

        }
        return false;
    }

    /**
     * Get a representation of the time for filenames.
     *
     * @return Representation of the time YYYY-MM-DD_HH-MM-SS-MMM.
     */
    public String getTimeForFilename() {
        return super.get(Calendar.YEAR) + "-" + addZeros("" + (super.get(Calendar.MONTH) + 1), 2) + "-" + addZeros("" + super.get(Calendar.DAY_OF_MONTH), 2) + "_" + addZeros("" + super.get(Calendar.HOUR_OF_DAY), 2) + "-" + addZeros("" + super.get(Calendar.MINUTE), 2) + "-" + addZeros("" + super.get(Calendar.SECOND), 2) + "-"
                + addZeros("" + super.get(Calendar.MILLISECOND), 3);
    }

    /**
     * Create a new instance of a calendar from the output of toString of
     * another instance.
     *
     * Note that it's not nullsafe
     *
     * @param toString output of toString of another instance.
     * @return new instance.
     */
    public static MyOwnCalendar getInstanceFromToString(String toString) {
        return new MyOwnCalendar(new MyOwnCalendar().reverseToStringToMillis(toString));
    }

    /**
     * Set the time to this calendar from the output of toString of another
     * instance.
     *
     * @param toString output of toString of another instance.
     * @return true if time was setted, false otherwise.
     */
    public boolean reverseToString(String toString) {
        Long value = reverseToStringToMillis(toString);
        if (value != null) {
            this.setTimeInMillis(value);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the time in millis represented by the of toString of another
     * instance.
     *
     * @param toString output of toString of another instance.
     * @return Time in millies that represent the time or null if it can be
     * calculated.
     */
    public static Long reverseToStringToMillis(String toString) {
        Long value = null;
        if (toString.length() == "[YYYY/MM/DD HH:MM:SS-MMM]".length()) {
            String copy = toString.substring(1, toString.length() - 1); //delete '[' and ']'
            String parts[] = copy.split("\\s"); //split by spaces and get [0]->YYYY/MM/DD and [1]->HH:MM:SS-MMM
            if (parts.length == 2) {
                String[] zeros = parts[0].split("/");
                String[] preones = parts[1].split(":");
                if (zeros.length == 3 && preones.length == 3) {
                    String[] s_m = preones[2].split("-");
                    if (s_m.length == 2) {
                        try {
                            Integer year = Integer.parseInt(zeros[0]);
                            Integer month = Integer.parseInt(zeros[1]);
                            Integer days = Integer.parseInt(zeros[2]);
                            Integer hours = Integer.parseInt(preones[0]);
                            Integer minutes = Integer.parseInt(preones[1]);
                            Integer seconds = Integer.parseInt(s_m[0]);
                            Integer millis = Integer.parseInt(s_m[1]);
                            MyOwnCalendar own = new MyOwnCalendar(0L);
                            own.set(year, month, days, hours, minutes, seconds);
                            return own.getTimeInMillis() + millis;
                        } catch (NumberFormatException ex) {

                        }
                    }
                }
            }
        }
        return value;
    }

    /**
     * Get the time in millis represented by the getFileTimeAsString of another
     * instance.
     *
     * @param toString output of getFileTimeAsString of another instance.
     * @return Time in millies that represent the time or null if it can be
     * calculated.
     */
    public static Long reverseGetFileTimeAsString(String toString) {
        Long value = null;
        if (toString.length() == "[YYYY/MM/DD HH:MM:SS]".length()) {
            String copy = toString.substring(1, toString.length() - 1); //delete '[' and ']'
            String parts[] = copy.split("\\s"); //split by spaces and get [0]->YYYY/MM/DD and [1]->HH:MM:SS-MMM
            if (parts.length == 2) {
                String[] zeros = parts[0].split("/");
                String[] preones = parts[1].split(":");
                if (zeros.length == 3 && preones.length == 3) {

                    try {
                        Integer year = Integer.parseInt(zeros[0]);
                        Integer month = Integer.parseInt(zeros[1]);
                        Integer days = Integer.parseInt(zeros[2]);
                        Integer hours = Integer.parseInt(preones[0]);
                        Integer minutes = Integer.parseInt(preones[1]);
                        Integer seconds = Integer.parseInt(preones[2]);
                        MyOwnCalendar own = new MyOwnCalendar(0L);
                        own.set(year, month, days, hours, minutes, seconds);
                        return own.getTimeInMillis();
                    } catch (NumberFormatException ex) {

                    }

                }
            }
        }
        return value;
    }

    /**
     * Get a representation of the time [YYYY/MM/DD HH/MM/SS]
     *
     * @return representation
     */
    public String getFileTimeAsString() {
        return "[" + addZeros("" + super.get(Calendar.YEAR), 4) + "/" + addZeros("" + (super.get(Calendar.MONTH) + 1), 2) + "/" + addZeros("" + super.get(Calendar.DAY_OF_MONTH), 2) + " "
                + addZeros("" + super.get(Calendar.HOUR_OF_DAY), 2) + ":" + addZeros("" + super.get(Calendar.MINUTE), 2) + ":" + addZeros("" + super.get(Calendar.SECOND), 2) + "]";
    }

    /**
     * Get a representation of the time
     *
     * @return Representation of the time [YYYY/MM/DD HH:MM:SS-MMM].
     */
    @Override
    public String toString() {
        return "[" + addZeros("" + super.get(Calendar.YEAR), 4) + "/" + addZeros("" + (super.get(Calendar.MONTH) + 1), 2) + "/" + addZeros("" + super.get(Calendar.DAY_OF_MONTH), 2) + " "
                + addZeros("" + super.get(Calendar.HOUR_OF_DAY), 2) + ":" + addZeros("" + super.get(Calendar.MINUTE), 2) + ":" + addZeros("" + super.get(Calendar.SECOND), 2) + "-"
                + addZeros("" + super.get(Calendar.MILLISECOND), 3) + "]";
    }
}
