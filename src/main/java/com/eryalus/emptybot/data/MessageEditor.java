/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eryalus.emptybot.data;

import java.util.ArrayList;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

/**
 *
 * @author eryalus
 */
public class MessageEditor {

    public static SendMessage removeTeclado(SendMessage m) {
        ReplyKeyboardRemove keyboardMarkup = new ReplyKeyboardRemove();
        m.setReplyMarkup(keyboardMarkup);
        return m;
    }

    public static SendMessage setTecladoSalir(SendMessage m) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        ArrayList<KeyboardRow> al = new ArrayList<>();
        KeyboardRow r = new KeyboardRow();
        r.add("Salir");
        al.add(r);
        keyboardMarkup.setKeyboard(al);
        m.setReplyMarkup(keyboardMarkup);
        return m;
    }

}
